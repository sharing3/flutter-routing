import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:fluro_routing/pages/emergency/emergency.dart';
import 'package:fluro_routing/menus/menu.dart';
import 'package:fluro_routing/pages/help/help.dart';
import 'package:fluro_routing/pages/profile/profile.dart';

class FluroRouter {
  static Router router = Router();
  // Handler for Page
  static Handler _menuHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => MainMenuPage());
  static Handler _emergencyHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => EmergencyPage());
  static Handler _helpHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => HelpPage());
  static Handler _profileHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => ProfilePage());

  static void setupRouter() {
    router.define(
      'menu',
      handler: _menuHandler,
    );
    router.define(
      'emergency',
      handler: _emergencyHandler,
    );
    router.define(
      'help',
      handler: _helpHandler,
    );
    router.define(
      'profile',
      handler: _profileHandler,
    );
  }
}
