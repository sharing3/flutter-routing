import 'package:flutter/services.dart';
import 'package:fluro_routing/router/route.dart'; //Customize the package name with the name of your project folder
import 'package:flutter/material.dart';

Future main() async {
  
  // Add this here to initialize the routes
  FluroRouter.setupRouter();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: 'menu',
    onGenerateRoute: FluroRouter.router.generator
  ));
}
